import json
from datetime import datetime, timedelta


class PiazzaUser:
    def __init__(self, name, piazza_id, computing_id):
        self.name = name
        self.piazza_id = piazza_id
        self.computing_id = computing_id
        self.number_posts = 0


def read_json(json_file_name):
    try:
        with open(json_file_name) as json_file:
            json_data = json_file.read()
        return json.loads(json_data)
    except IOError:
        print('The file "{}" does not exist. Did you place it in the right spot?'.format(json_file_name))
        exit(1)


def make_user_list(user_objects, ignored_users):
    user_list = []
    for user_object in user_objects:
        user_computing_id = user_object['email'].split('@')[0]
        if user_computing_id not in ignored_users:
            user = PiazzaUser(user_object['name'], user_object['user_id'], user_computing_id)
            user_list.append(user)
    user_list.sort(key=lambda x: x.name)
    return user_list


def make_user_dictionary(user_list):
    user_dict = {}
    for user in user_list:
        user_dict[user.piazza_id] = user
    return user_dict


def count_posts(user_dict, post_objects, start_date, end_date):
    for post in post_objects:
        if 'created' in post.keys():
            post_date = datetime.strptime(post['created'], '%Y-%m-%dT%H:%M:%S.%fZ').date()
            # Count posts whose dates lie between start_date (inclusive) and end_date (exclusive)
            if start_date <= post_date < end_date:
                post_editors = post['editors']
                # Remove duplicate editors. Repeated edits do not count as contributions.
                post_editors = list(dict.fromkeys(post_editors))
                for editor in post_editors:  # Multiple people can edit a post, Piazza counts these as contributions.
                    if editor in user_dict.keys():
                        user = user_dict[editor]
                        user.number_posts = user.number_posts + 1


def output_to_csv(user_list, output_file_name):
    with open(output_file_name, 'w') as output_file:
        output_file.write('Name, Computing ID, Number of Posts\n')
        for user in user_list:
            output_file.write('{},{},{}\n'.format(user.name, user.computing_id, user.number_posts))


if __name__ == '__main__':
    # Editable stuff
    # Only change the following two lines if you renamed the json files from Piazza.
    classContentFileName = 'class_content_flat.json'
    piazzaUserFileName = 'users.json'
    ignoredUsers = []  # Fill this list with any instructor computing IDs, the script will ignore these
    endDate = datetime.today().date()  +timedelta(days=1) # This should be the day after you want the count to stop
    # endDate = datetime.fromisoformat('yyyy-mm-dd')  # If you want to specify a day, uncomment this and edit the date
    startDate = endDate - timedelta(days=7)  # This sets the beginning of the window a week before the endDate
    # startDate = datetime.fromisoformat('yyyy-mm-dd')  # If you want to specify a day, uncomment this and edit the date
    # Feel free to change this to whatever
    outputFileName = 'piazza_digest_week_{}--{}.csv'.format(startDate, endDate - timedelta(days=1))
    # End editable stuff

    postObjects = read_json(classContentFileName)
    userObjects = read_json(piazzaUserFileName)

    userList = make_user_list(userObjects, ignoredUsers)

    userDictionary = make_user_dictionary(userList)

    count_posts(userDictionary, postObjects, startDate, endDate)

    output_to_csv(userList, outputFileName)

# `collab_roster_parse.py`

Takes the roster that you downloaded from Collab (in CSV format) and produces a roster (in CSV format) with separate columns for Email, Computing ID, Last Name, and First Name.
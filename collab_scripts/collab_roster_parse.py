import csv

inputFileName = 'roster.csv'
outputFileName = 'seproster.csv'
with open(inputFileName) as csvfile:
    next(csvfile)
    reader = csv.reader(csvfile, quotechar='"')
    with open(outputFileName,'w') as csvout:
        writer = csv.writer(csvout)
        writer.writerow(['Last Name', 'First Name', 'Computing ID', 'Email'])
        for row in reader:
            namesplit = row[1].split(',')
            fname = namesplit[1].strip()
            lname = namesplit[0].strip()
            computingID = row[0]
            email = computingID + '@virginia.edu'
            writer.writerow([lname, fname, computingID, email])

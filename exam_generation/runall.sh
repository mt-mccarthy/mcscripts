#!/bin/bash
for roster in $(ls *.csv)
do
    rostername=$(basename -s .csv $roster)
    python ./generate_exams.py $roster "Oral Exam 2" --examout $rostername-exams
    # Make a temporary directory
    mkdir build
    cp uva-oralexam.cls $rostername-exams
    cp commands.sty $rostername-exams
    pushd $rostername-exams
    rm *.pdf
    # Compile all tex files
    for f in $(ls *.tex)
    do
        latexmk -interaction="batchmode" -pdf -output-directory="../build" "$f"
    done
    popd
    pushd build
    # Move all the PDF files back to the original directory
    for f in $(ls *.pdf)
    do
        mv $f ../$rostername-exams
    done
    popd
    # Clean up
    rm -rf build
done

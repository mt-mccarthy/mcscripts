#!/bin/bash
DIRECTORIES=( roster-900-exams ) # Directories that contain tex files you wish to build
for direc in ${DIRECTORIES[@]}
do
    # Make a temporary directory
    mkdir build-$direc
    cd $direc
    rm *.pdf
    # Compile all tex files
    for f in $(ls *.tex)
    do
        latexmk -interaction="batchmode" -pdf -output-directory="../build-$direc" "$f"
    done
    cd ../build-$direc
    # Move all the PDF files back to the original directory
    for f in $(ls *.pdf)
    do
        mv $f ../$direc
    done
    cd ..
    # Clean up
    rm -rf build-$direc
done

# `generate_exams.py`

## How to call

To use this script run the following command
```bash
python generate_exams.py path/to/roster.csv "Exam Title" --bankout path/to/bankfolder --examout path/to/examfolder --omit path/to/omit.csv
```

The `--bankout`, `--examout`, and `--omit` are optional arguments and need not be called.

| Argument | Description |
| -------- | ----------- |
| roster | A CSV file filled with student names |
| Exam Title | What the exam should be titled |
| bankout | The folder in which to place the question bank |
| examout | The folder in which to place the produced exams |
| omit | A CSV file containing a list of question options to omit from the exams |

## Input

The roster should be a CSV file with student names. Name format is `last,first`.

Questions are subfolders of the `questions` folder. The possible options for a question are `.tex` files that are placed inside the subfolder.
For example if `a.tex` is an option for `question1`, `a.tex` should be placed inside the `question1` folder.

This script has support for including rubrics in the question bank tex as well as at the end of each exam. Rubrics must go in the corresponding
folder inside the `rubrics` folder. For example a rubric for the question `questions/question1/a.tex` should be titled `a.tex` and placed in the
`rubrics/question1` folder. That is, its file path should be `rubrics/question1/a.tex`.

We can also omit options from consideration by passing a CSV file to the `--omit` option. For example suppose `question1` has options `a,b,c,d,e` and
`question2` has options `a,b,c`. If we want to remove options `a,c,e` of `question1` and option `c` of `question2` from consideration, the omit CSV
would look like this:
```
question1,a,c,e
question2,c
```
Passing the filepath to this omit file will make the question bank options `b` and `d` for `question1`, and options `a` and `b` for `question2`.

## Output

Question bank (`.tex` file, both with article class as well as beamer class) in `bank` folder (by default) or in the `bankout` folder.

An exam (`.tex` file) for each student in the input CSV. These are placed in the `exams` folder (by default) or in the `examout` folder.

To compile all exams, run the `compile_exams.sh` file or use `make` (TODO: write the makefile). This should work on Linux and Mac.
It *might* work on Windows depending on whether or not a Linux shell (like `bash`) and `latexmk` are in your PATH. This will certainly
work with Windows Subsystem for Linux if you have that installed and have TeXlive installed in that.

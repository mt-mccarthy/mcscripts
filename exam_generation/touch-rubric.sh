for D in `find ./questions -mindepth 1 -type d`
do
	dirname=${D##*/}
	for F in `ls $D`
	do
		touch "rubrics/$dirname/$F"
	done
done

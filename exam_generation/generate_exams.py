import argparse
import os
import random


def read_roster(csvfilename):
    """ Reads the roster from a CSV File

    Parameters
    ----------
    csvfilename -- The path to a CSV file

    Returns
    -------
    roster
        A list of pairs of the form (lname, fname) where 'lname,fname' are lines in the CSV file.
    """
    roster = []
    with open(csvfilename) as csvfile:
        for line in csvfile:
            name_pair = [name.strip() for name in line.strip().split(',')]
            roster.append(tuple(name_pair))
    return roster


def read_omit_file(csvfilename):
    """ Reads the list of omitted questions from a CSV File

    Parameters
    ----------
    csvfilename -- The path to a CSV file

    Returns
    -------
    omit_me
        A list of pairs of the form (question, option) where option is an option to be ignored for the question.
    """
    omit_me = []
    with open(csvfilename) as csvfile:
        for line in csvfile:
            entries = [entry.strip() for entry in line.strip().split(',')]
            for entry in entries[1:]:
                omit_me.append((entries[0], entry))
    return omit_me


def generate_question_list(questiondir):
    """ Walks the question directory to form a list of all questions and options.

    Parameters
    ----------
    questiondir -- A directory containing directories of question options.

    Returns
    -------
    question_list
        A list of tuples of the form (dir, [option1, option2, ...]) where dir is a directory in questiondir
        and each optionJ is a tex file in questiondir/dir.
    """
    question_list = []
    for qdir in os.listdir(questiondir):
        cdir = questiondir + os.path.sep + qdir
        # Ignore non-directories
        if os.path.isdir(cdir):
            # List of all tex files in the question directory, cdir
            question_options = [question[0:-4] for question in os.listdir(
                cdir) if os.path.isfile(os.path.join(cdir, question)) and question.endswith('.tex')]
            question_list.append((qdir, sorted(question_options)))
    question_list.sort(key=lambda question: question[0])
    return question_list


def question_list_to_dictionary(question_list):
    """ Converts the question list into a dictionary for easier handling

    Parameters
    ----------
    questionlist -- The output of generate_question_list

    Returns
    -------
    q_dict
        A dictionary mapping each qdir (category) to its list of options
    """
    q_dict = {}
    for (qdir, options) in question_list:
        q_dict[qdir] = []
        for option in options:
            q_dict[qdir].append(option)
    return q_dict


def remove_omitted_questions(question_list, omit_list):
    """ Walks the question directory to form a list of all questions and options.

    Parameters
    ----------
    question_list -- The output of generate_question_list
    omit_list -- The output of read_omit_file

    Returns
    -------
    thin_question_list
        A list of tuples of the form (dir, [option1, option2, ...]) where dir is a directory in questiondir
        and each optionJ is a tex file in questiondir/dir. Each of the options is not in the omit list.
    """
    thin_question_list = []
    for (qdir, options) in question_list:
        # Include all options for this question not in the omit_list
        thin_options = [option for option in options if (
            qdir, options) not in omit_list]
        thin_question_list.append(qdir, sorted(thin_options))
    thin_question_list.sort(key=lambda question: question[0])
    return thin_question_list


def choose_random_options(question_list):
    """ Chooses a random option for each question.

    Parameters
    ----------
    question_list -- The output of generate_question_list

    Returns
    -------
    choices
        A list of pairs of the form (dir, option) where option is a tex file in the dir subdirectory of the questions directory
    """
    choices = []
    for (qdir, options) in question_list:
        choice = random.choice(options)
        choices.append((qdir, choice))
    return choices


def choose_random_options_e1(question_list):
    """ Chooses a random option for each question.

    Parameters
    ----------
    question_list -- The output of generate_question_list

    Returns
    -------
    choices
        A list of pairs of the form (dir, option) where option is a tex file in the dir subdirectory of the questions directory.
        One question directory gets assigned a problem from the study guide, the rest get non-study guide problems.
    """
    # Choose extra conceptual category
    categories = [qdir for (qdir, options)
                  in question_list if qdir not in ['1-student-sg', '2-ivt-squeeze', '3-conceptual', '4-diffrules']]
    conceptual_choice = random.choice(categories)

    choices = []
    for (qdir, options) in question_list:
        choice = ''
        new_options = []
        if qdir == conceptual_choice:
            new_options = [
                option for option in options if option.startswith('conc')]
        else:
            new_options = [
                option for option in options if not option.startswith('conc')]
        choice = random.choice(new_options)
        choices.append((qdir, choice))
    choices2 = choices[0:3]
    choices3 = [(qdir, option) for (qdir, option) in choices if
                qdir not in ['1-student-sg', '2-ivt-squeeze', '3-conceptual', conceptual_choice]]
    random.shuffle(choices3)
    choices2.extend(choices3)
    choices2.extend([(qdir, option)
                     for (qdir, option) in choices if qdir == conceptual_choice])

    return choices2


def choose_random_options_e2(question_dict):
    """ Chooses a random option for each question.

    Parameters
    ----------
    question_dict -- The output of question_list_to_dictionary

    Returns
    -------
    choices
        A list of pairs of the form (dir, option) where option is a tex file in the dir subdirectory of the questions directory.
        One question directory gets assigned a problem from the study guide, the rest get non-study guide problems.
    """
    # Student study guide category
    stu_cat = '1-sg-lhopital'
    # Number of conceptual questions to ask
    num_conc = 2
    # Choose second sg category
    sg_cat = random.choice(['3-relatedrates', '5-optimization'])
    # Create list of non-study guide categories
    nonsg_cats = [qdir for qdir in question_dict.keys() if qdir not in [
        stu_cat, sg_cat]]
    # Choose conceptual categories
    nonconceptual_cats = ['4-impdiff']
    conceptual_choices = random.sample([qdir for qdir in nonsg_cats
                                        if qdir not in nonconceptual_cats], k=num_conc)
    other_cats = [
        qdir for qdir in nonsg_cats if qdir not in conceptual_choices]
    random.shuffle(conceptual_choices)
    random.shuffle(other_cats)

    # Create an order for the categories
    # Right now the order is 1) student choice 2) study guide question 3) nonconceptual 1 4) conceptual 1
    # 5) nonconceptual 2 6) conceptual 2
    cats = [stu_cat, sg_cat, other_cats[0], conceptual_choices[0],
            other_cats[1], conceptual_choices[1]]

    choices = []
    for qdir in cats:
        options = []
        # If the current category is the study guide category, pick a study guide question
        if qdir in [stu_cat, sg_cat]:
            options = [
                option for option in question_dict[qdir] if option.startswith('sg')]
        # If the current category is selected as one of the conceptual categories, pick a conceptual question.
        elif qdir in conceptual_choices:
            options = [
                option for option in question_dict[qdir] if option.startswith('conc')]
        # If the current category is neither of the above, pick a non-sg and non-conceptual question
        else:
            options = [
                option for option in question_dict[qdir] if not (option.startswith('sg') or option.startswith('conc'))]
        choices.append((qdir, random.choice(options)))

    return choices


def create_bank_tex(output_dir, question_list):
    """ Creates a tex file with all of the questions written in it.

    Parameters
    ----------
    output_dir -- The directory in which to drop this tex file.
    question_list -- The output of generate_question_list
    """
    bank_file = output_dir + os.path.sep + 'bank.tex'
    os.makedirs(os.path.dirname(bank_file), exist_ok=True)
    with open(bank_file, 'w') as bank_tex:
        bank_tex.write('\\documentclass{../bank}\n')
        bank_tex.write('\\begin{document}\n')
        for (qdir, opts) in question_list:
            # One section per question
            bank_tex.write('\\section{{{}}}\n'.format(qdir))
            for opt in opts:
                # Subsection for each possible choice
                bank_tex.write('\\subsection{{{}}}\n'.format(opt))
                # Location of the tex file for this choice
                bank_tex.write(
                    'File: \\verb|questions/{}/{}.tex|\n\n'.format(qdir, opt))
                # This macro inserts the question into the document
                bank_tex.write(
                    '\\insertquestioninbank{{{}}}{{{}}}\n\n'.format(qdir, opt))
                # Location of the tex file for this rubric
                bank_tex.write(
                    'File: \\verb|rubrics/{}/{}.tex|\n\n'.format(qdir, opt))
                # This macro inserts the rubric into the document
                bank_tex.write(
                    '\\insertrubricinbank{{{}}}{{{}}}\n\\newpage\n'.format(qdir, opt))
        bank_tex.write('\\end{document}')


def create_exam_tex(output_dir, student_fname, student_lname, exam_title, question_choices):
    """ Creates a tex file for an exam with a single choice per question.

    Parameters
    ----------
    output_dir -- The directory in which to drop this tex file.
    student_fname -- The first name of the student that will take this exam.
    student_lname -- The last name of the student that will take this exam.
    exam_title -- The title of the exam.
    question_choices -- A list of pairs of the form (dir, choice) where dir is contained in the questions directory and choice is a file inside dir.
    """
    exam_file = output_dir + os.path.sep + \
        '{}-{}.tex'.format(student_lname.replace(" ", ""),
                           student_fname.replace(" ", ""))
    os.makedirs(os.path.dirname(exam_file), exist_ok=True)
    with open(exam_file, 'w') as exam_tex:
        exam_tex.write('\\documentclass[10pt]{uva-oralexam}\n')
        exam_tex.write('\\author{{{} {}}}\n'.format(
            student_fname, student_lname))
        exam_tex.write('\\title{{{}}}\n\n'.format(exam_title))
        qcounter = 1
        for (qdir, qchoice) in question_choices:
            exam_tex.write('% Question {}\n'.format(qcounter))
            # Note that chr(65)='A', so chr(64+qcounter) is the qcounter-th letter of the alphabet
            exam_tex.write('\\def\\category{}{{{}}}\n'.format(
                chr(64+qcounter), qdir))
            exam_tex.write(
                '\\def\\question{}{{{}}}\n\n'.format(chr(64+qcounter), qchoice))
            qcounter = qcounter + 1
        exam_tex.write('\\begin{document}\n')
        exam_tex.write('\\frame{\\titlepage\\honorcode}\n\n')
        qcounter = 1
        # One choice for each question
        for (qdir, qchoice) in question_choices:
            exam_tex.write('\\begin{frame}\n')
            exam_tex.write('\\frametitle{{Question {}}}\n'.format(qcounter))
            exam_tex.write(
                '\\insertquestion{{\\category{}}}{{\\question{}}}\n'.format(chr(64+qcounter), chr(64+qcounter)))
            exam_tex.write('\\end{frame}\n\n')
            qcounter = qcounter + 1
        exam_tex.write('\\begin{frame}\n')
        exam_tex.write('\\begin{center}\n')
        exam_tex.write('\\begin{LARGE}\n')
        exam_tex.write('End of Exam\n')
        exam_tex.write('\\end{LARGE}\n')
        exam_tex.write('\\end{center}\n')
        exam_tex.write('\\end{frame}\n\n')
        # Insert rubrics at end of exam tex
        qcounter = 1
        # Reprint the question along with the rubric
        for (qdir, qchoice) in question_choices:
            exam_tex.write('\\begin{frame}\n')
            exam_tex.write(
                '\\frametitle{{Q{}: rubrics/\\category{}/\\question{}}}\n'.format(qcounter, chr(64+qcounter), chr(64+qcounter)))
            exam_tex.write(
                '\\insertrubric{{\\category{}}}{{\\question{}}}\n'.format(chr(64+qcounter), chr(64+qcounter)))
            exam_tex.write('\\end{frame}\n\n')
            qcounter = qcounter + 1
        exam_tex.write('\\end{document}')


def create_exam_list_tex(output_dir, exam_title, question_list):
    """ Creates a tex file for an exam with all question choices per question.

    Parameters
    ----------
    output_dir -- The directory in which to drop this tex file.
    exam_title -- The title of the exam.
    question_list -- The output of generate_question_list
    """
    exam_file = output_dir + os.path.sep + 'bank-slides.tex'
    os.makedirs(os.path.dirname(exam_file), exist_ok=True)
    with open(exam_file, 'w') as exam_tex:
        exam_tex.write('\\documentclass[10pt]{../uva-oralexam}\n')
        exam_tex.write('\\title{{{}}}\n'.format(exam_title))
        exam_tex.write('\\begin{document}\n')
        exam_tex.write('\\frame{\\titlepage}\n\n')

        for (qdir, choices) in question_list:
            exam_tex.write('\\section{{{}}}\n\n'.format(qdir))
            for qchoice in choices:
                exam_tex.write('\\subsection{{{}}}\n\n'.format(qchoice))
                exam_tex.write('\\begin{frame}\n')
                exam_tex.write(
                    '\\frametitle{{questions/{}/{}}}\n'.format(qdir, qchoice))
                exam_tex.write(
                    '\\insertquestion{{{}}}{{{}}}\n'.format(qdir, qchoice))
                exam_tex.write('\\end{frame}\n\n')
                exam_tex.write('\\begin{frame}\n')
                exam_tex.write(
                    '\\frametitle{{rubrics/{}/{}}}\n'.format(qdir, qchoice))
                # exam_tex.write(
                #     '\\insertquestioninrubric{{{}}}{{{}}}\n'.format(qdir, qchoice))
                exam_tex.write(
                    '\\insertrubric{{{}}}{{{}}}\n'.format(qdir, qchoice))
                exam_tex.write('\\end{frame}\n\n')
        exam_tex.write('\\end{document}')


def make_exams(roster, title, omit='', bankout='bank', examout='exams'):
    roster = read_roster(roster)
    question_list = generate_question_list('questions')
    question_dict = question_list_to_dictionary(question_list)

    if omit != '':
        if os.path.isfile(omit):
            question_list = remove_omitted_questions(
                question_list, read_omit_file(omit))
        else:
            print('{} does not exist or is not a file'.format(omit))

    create_bank_tex(bankout, question_list)
    create_exam_list_tex(bankout, title, question_list)

    for (lname, fname) in roster:
        create_exam_tex(examout, fname, lname, title,
                        choose_random_options_e2(question_dict))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'roster', type=str, help='A CSV file containing the roster to produce exams for.')
    parser.add_argument('title', type=str,
                        help='The title of the exam.')
    parser.add_argument('--bankout', type=str,
                        help='The directory to store the question bank tex.', default='bank')
    parser.add_argument('--examout', type=str,
                        help='The directory to store the exam tex.', default='exams')
    parser.add_argument(
        '--omit', type=str, help='A CSV file with a list of question options to not consider.', default='')
    args = parser.parse_args()

    make_exams(args.roster, args.title, omit=args.omit,
               bankout=args.bankout, examout=args.examout)

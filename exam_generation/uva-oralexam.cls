\ProvidesClass{uva-oralexam}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions \relax

\LoadClass{beamer}

\usepackage{commands}

\usetheme{Boadilla}
%	\useoutertheme{split}
	\setbeamertemplate{navigation symbols}{}
	\usecolortheme{whale}
		\definecolor{UniOrange1}{RGB}{229,114,0}
		\definecolor{UniOrange2}{RGB}{243,115,28}
		\definecolor{UniBlue1}{RGB}{20,30,60}
		\definecolor{UniBlue2}{RGB}{4,35,91}
		\definecolor{ExBlue}{RGB}{100,149,237}
		\setbeamercolor{title}{bg=UniBlue2,fg=UniOrange2}
		\setbeamercolor{frametitle}{bg=UniBlue2,fg=UniOrange2}
		\setbeamercolor{structure}{bg=UniBlue2,fg=UniOrange2}
		\setbeamercolor{palette primary}{bg=UniBlue2,fg=UniOrange2}
		\setbeamercolor{palette secondary}{bg=UniBlue2,fg=UniOrange2}
		\setbeamercolor{palette tertiary}{bg=UniBlue2,fg=UniOrange2}
		\setbeamercolor{palette quaternary}{bg=UniBlue2,fg=UniOrange2}


\setbeamertemplate{page number in head/foot}{}
\setbeamertemplate{itemize/enumerate subbody begin}{\scriptsize}
\setbeamertemplate{itemize/enumerate subsubbody begin}{\scriptsize}

\def\honorcode{\Large\centering On my honor, I pledge that I have neither given nor received help on this assignment.}
